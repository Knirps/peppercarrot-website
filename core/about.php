<?php if ($root=="") exit;


# Include the language selection menu and credit engine
include($file_root.'core/mod-menu-lang.php');
include($file_root.'core/lib-credits.php');

$license_link = ''.$root.'/'.$lang.'/license/index.html';

echo '  <div style="clear:both"></div>'."\n";
echo '<div class="container" style="max-width: 1160px;">'."\n";
echo '  <section class="about col sml-12 med-12 lrg-10 sml-centered"'."\n";
echo '>'."\n";

echo '  <center><img src="'.$root.'/'.$sources.'/0ther/website/hi-res/2021-07-14_author_by-David-Revoy.jpg" title="'._("Photo of the author, David Revoy with his cat named Noutti while drawing the first episodes of Pepper&Carrot.").'" align="center"></center>'."\n";

echo '  <h2>'._("The author").'</h2> '."\n";

echo '  <p>'._("Hi, my name is David Revoy and I'm a French artist born in 1981. I'm self-taught and passionate about drawing, painting, cats, computers, Gnu/Linux open-source culture, Internet, old school RPG video-games, old mangas and anime, traditional art, Japanese culture, fantasy…").'</p>'."\n";
echo '  <p>'._("After more than 10 years of freelance in digital painting, teaching, concept-art, illustrating and art-direction, I decided to start my own project. I finally found a way to mix all my passions together, the result is Pepper&Carrot.").'</p> '."\n";
echo '  <p>'._("I'm working on this project since May 2014, and I received the help of many contributors and supporters on the way. ").' '."\n";
echo '  '.sprintf(_("You'll find the full list of credits on the <a href=\"%s\">License menu</a>."),$license_link).'</p> '."\n";
echo ''."\n";
echo '  <p>'._("My blog: ").' <a href="https://www.davidrevoy.com">www.davidrevoy.com</a><br/> '."\n";
echo '     '._("My email: ").' <a href="mailto:info@davidrevoy.com">info@davidrevoy.com</a></p> '."\n";

echo '  <br/>';

echo '  <h1 id="philosophy">'._("The philosophy of Pepper&Carrot").'</h1> '."\n";

echo '  <h2>'._("Supported by patrons").'</h2> '."\n";

echo '  <p>'._("Pepper&Carrot project is only funded by its patrons, from all around the world. Each patron sends a little money for each new episode published and gets a credit at the end of the new episode. Thanks to this system, Pepper&Carrot can stay independent and never have to resort to advertising or any marketing pollution.").'</p> '."\n";

_img($sources.'/0ther/website/hi-res/2015-02-09_philosophy_01-support_by-David-Revoy.jpg', _("Pepper and Carrot receiving money from the audience."), 1200, 740, 82);

echo '  <h2>'._("100&#37; free(libre), forever, no paywall").'</h2> '."\n";

echo '  <p>'._("All the content I produce about Pepper&Carrot is on this website or on my blog, free(libre) and available to everyone. I respect all of you equally: with or without money. All the goodies I make for my patrons are also posted here. Pepper&Carrot will never ask you to pay anything or to get a subscription to get access to new content.").'</p> '."\n";

_img($sources.'/0ther/website/hi-res/2015-02-09_philosophy_03-paywall_by-David-Revoy.jpg', _("Carrot, locked behind a paywall."), 1200, 740, 82);


echo '  <h2>'._("Open-source and permissive").'</h2> '."\n";

echo '  <p>'._("I want to give people the right to share, use, build and even make money upon the work I've created. All pages, artworks and content were made with Free(Libre) Open-Sources Software on GNU/Linux, and all sources are on this website (Sources and License buttons). Commercial usage, translations, fan-art, prints, movies, video-games, sharing, and reposts are encouraged. You just need to give appropriate credit to the authors (artists, correctors, translators involved in the artwork you want to use), provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the authors endorse you or your use. More information can be read about it here: ").''."\n";

# End of link for language selection of https://creativecommons.org/licenses/by/4.0/
# %s stands for the 2 language code letters, in some cases it doesn't match the licence.
# If that's the case, you can replace %s with a static code, e.g. deed.es or deed.pt
# Doing this may cause an error in some editors, ignore it.
echo '  <a href="https://creativecommons.org/licenses/by/4.0/'.sprintf(_("deed.%s"), $lang).'">'._("Creative Commons Attribution 4.0 International license.").'</strong></a><br/>'."\n";'</p> '."\n";

_img($sources.'/0ther/website/hi-res/2015-02-09_philosophy_04-open-source_by-David-Revoy.jpg', _("Example of derivatives possible."), 1200, 740, 82);

echo '  <h2>'._("Quality entertainment for everyone, everywhere").'</h2> '."\n";

echo '  <p>'._("Pepper&Carrot is a comedy/humor webcomic suited for everyone, every age. No mature content, no violence. Free(libre) and open-source, Pepper&Carrot is a proud example of how cool free-culture can be. I focus a lot on quality, because free(libre) and open-source doesn't mean bad or amateur. Quite the contrary.").'</p> '."\n";

_img($sources.'/0ther/website/hi-res/2015-02-09_philosophy_05-everyone_by-David-Revoy.jpg', _("Comic pages around the world."), 1200, 740, 82);

echo '  <h2>'._("Let's change comic industry!").'</h2> '."\n";

echo '  <p>'._("Without intermediary between artist and audience you pay less and I benefit more. You support me directly. No publisher, distributor, marketing team or fashion police can force me to change Pepper&Carrot to fit their vision of 'the market'. Why couldn't a single success 'snowball' to a whole industry in crisis? We'll see…").'</p> '."\n";

_img($sources.'/0ther/website/hi-res/2015-02-09_philosophy_06-industry-change_by-David-Revoy.jpg', _("Diagram: on the left-hand side, Carrot is losing money with many middle-men. On the right-hand side, the result is more balanced."), 1200, 740, 82);

echo '   <a class="buttonpage" style="font-size: 1.2rem; padding: 1.5rem 3rem; background-color:#F86754" href="'.$root.'/'.$lang.'/support/index.html"> '."\n";
echo '     '._("Become a patron").' '."\n";
echo '   </a> '."\n";

echo '  </section>'."\n";
echo ''."\n";
echo '  <div style="clear:both"></div>'."\n";
echo '  <br>'."\n";
echo '  <br>'."\n";
echo '  <br>'."\n";
echo '</div>'."\n";
echo ''."\n";

?>
