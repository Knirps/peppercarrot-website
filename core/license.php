<?php if ($root=="") exit;

echo '<div class="container">'."\n";

# Include the language selection menu and credit engine
include($file_root.'core/mod-menu-lang.php');
include($file_root.'core/lib-credits.php');

echo '	<section class="page col pad sml-12 sml-text-center" style="text-align:center;"'."\n";

echo '>'."\n";

echo '  <h1>'._("License:").'</h1>'."\n";
echo '  <a href="https://creativecommons.org/licenses/by/4.0/'.sprintf(_("deed.%s"), $lang).'"><img src="'.$root.'/core/img/ccby-large.jpg" style="margin-top: 25px;"/></a><br/>'."\n";
# A note here, because the Copyright © will probably rise eyebrows on a Free/Libre and Open-source Project.
# It is advised to declare a 'copyright holder' before also declaring releasing as Creative Commons for some copyright code of some countries.
;
# Note to translators: The %s placeholders are an opening and a closing link tag
echo '  '.sprintf(_('This work is licensed under a %sCreative Commons Attribution 4.0 International license%s'), '<strong><a href="https://creativecommons.org/licenses/by/4.0/'.sprintf(_("deed.%s"), $lang).'">', '</strong></a>')."<br/>\n";
echo '  '._("Copyright © David Revoy").' '.date("Y").', '."\n";
echo '  <a href="https://www.peppercarrot.com">www.peppercarrot.com</a><br/><br/>'."\n";
echo ''._("Here is the total list of attributions for the selected language:").''."\n";
echo '  <br/>'."\n";
echo '  <h1>'._("Full attribution:").'</h1>'."\n";
echo '  <h2>'.$languages_info[$lang]['local_name'].'</h2>'."\n";

echo '  <h3>'._("Project management:").'</h3>'."\n";
echo '  <br/>'."\n";
_print_global();

_print_website_translation($lang);

echo '  <h3>'._("Hereva worldbuilding:").'</h3>'."\n";
echo '  <br/>'."\n";
_print_hereva();

echo '  <h3>'._("Episodes:").' </h3>'."\n";
echo '  <br/>'."\n";

# Loop on the folders for headers
foreach($episodes_list as $epdirectory) {
  _print_title($lang, $epdirectory);
  echo '    '."\n";
  _print_credits($lang, $epdirectory);
  echo ''."\n";
}

echo '  <h1>'._("Special thanks to:").'</h1><br/>'."\n";
echo '  <strong>'._("All patrons of Pepper&amp;Carrot! Your support made all of this possible.").'</strong><br/>'."\n";
echo '  <br/>'."\n";
echo '  <strong>'._("All translators on Pepper&amp;Carrot for their contributions:").'</strong><br/>'."\n";
# Loop on the folders for headers
$alltranslators = array();
foreach($episodes_list as $key => $ep_directory) {
$lang_for_this_episode = $episodes_all_translations[$key];
  foreach($lang_for_this_episode as $langjson) {
    $pattern = ''.$sources.'/'.$ep_directory.'/lang/'.$langjson.'/info.json';
    $test = $pattern;
    if (file_exists($pattern)){
      $translatorinfos = json_decode(file_get_contents($pattern), true);
      if (isset($translatorinfos['credits'])) {
        if (isset($translatorinfos['credits']['translation'])) {
          foreach ($translatorinfos['credits']['translation'] as $translator) {
            $alltranslators[] = $translator;
          }
        }
      }
    }
  }
}

$result = array_unique($alltranslators);
$result = array_diff($result, array("original version"));
_print_translatorinfos($result, ", ", ".");
echo '<br/><br/>';
echo ''."\n";

# Special thanks to projects:
echo '  '._("<strong>The Framasoft team</strong> for hosting our oversized repositories via their Gitlab instance Framagit.").''."\n";
echo '  <br/><br/>'."\n";
echo ''."\n";

echo '  '._("<strong>The Libera.Chat staff and Matrix.org staff</strong> for our #pepper&carrot community channel.").''."\n";
echo '  <br/><br/>'."\n";
echo ''."\n";

echo '  '._("<strong>All Free/Libre and open-source software</strong> because Pepper&Carrot episodes are created using 100&#37; Free/Libre software on a GNU/Linux operating system. The main ones used in production being:").'<br/>'."\n";
echo '  '._("- <strong>Krita</strong> for artworks (krita.org).").'<br/>'."\n";
echo '  '._("- <strong>Inkscape</strong> for vector and speechbubbles (inkscape.org).").'<br/>'."\n";
echo '  '._("- <strong>Blender</strong> for artworks and video editing (blender.org).").'<br/>'."\n";
echo '  '._("- <strong>Kdenlive</strong> for video editing (kdenlive.org).").'<br/>'."\n";
echo '  '._("- <strong>Scribus</strong> for the book project (scribus.net).").'<br/>'."\n";
echo '  '._("- <strong>Gmic</strong> for filters and effects (gmic.eu).").'<br/>'."\n";
echo '  '._("- <strong>ImageMagick</strong> & <strong>Bash</strong> for 90&#37; of automation on the project.").'<br/>'."\n";
echo '  <br/>'."\n";
echo ''."\n";


$technicalinfos = json_decode(file_get_contents($sources.'/project-global-credits.json'), true);
echo '  <strong>'._("And finally to all developers who interacted on fixing Pepper&Carrot specific bug-reports:").'</strong> <br/>'."\n";
_print_translatorinfos($technicalinfos['project-global-credits']['bug-fix-heroes'], ", ", ".");
echo '  <br/><br/><strong>'._("... and anyone I've missed.").'</strong>'."\n";
echo '  <br/><br/>'."\n";
echo '  <br/><br/>'."\n";
echo ''."\n";

echo '  </section>'."\n";
echo ''."\n";
echo '  <div style="clear:both"></div>'."\n";
echo '  <br>'."\n";
echo '  <br>'."\n";
echo '  <br>'."\n";
echo '</div>'."\n";
echo ''."\n";

?>
