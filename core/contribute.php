<?php if ($root=="") exit; 

# Initiate page
echo '<div class="container" style="max-width: 1280px;">'."\n";

# Include the webcomic sources 
include($file_root.'core/mod-menu-lang.php');

echo '  <article id="blockmenu" class="col sml-12 sml-text-center">'."\n";

$ccbylink = 'https://creativecommons.org/licenses/by/4.0/'.sprintf(_("deed.%s"), $lang);
$chatroom = $root.'/'.$lang.'/chat/index.html';
echo '<p style="font-size: 1.1rem;">'.sprintf(_("Thanks to the <a href=\"%s\">Creative Commons license</a>, you can contribute Pepper&Carrot in many ways."), $ccbylink).'<br/>'."\n";
echo ''.sprintf(_("Join the <a href=\"%s\">Pepper&Carrot chat room</a> to share your projects!"),$chatroom).'</p>';
echo '<small>'._("(Note: English language is used accross our documentations, wiki and channels.)").'</small><br/>'."\n";

function _menu_item($picture_path, $title, $link) {
  global $root;
  global $sources;
  echo '  <figure class="col sml-12 med-6 blockitem">'."\n";
  echo '    <a href="'.$link.'">'."\n";
  echo '        <img src="'.$root.'/'.$sources.'/'.$picture_path.'" />'."\n";
  echo '      <figcaption class="blockcaption">'.$title.'</figcaption>'."\n";
  echo '    </a>'."\n";
  echo '  </figure>'."\n";
}

_menu_item("/0ther/website/hi-res/2021-07-14_contribute-A-derivation_by-David-Revoy.jpg", ''._("Your Derivations").'', 'https://www.davidrevoy.com/tag/derivation/');
_menu_item("/0ther/website/hi-res/2021-07-14_contribute-B-fan-art_by-David-Revoy.jpg", ''._("Fan-art Gallery").'', ''.$root.'/'.$lang.'/fan-art/artworks.html');
_menu_item("/0ther/website/hi-res/2021-07-14_contribute-C-translation_by-David-Revoy.jpg", ''._("Translation Documentation").'', ''.$root.'/'.$lang.'/documentation/010_Translate_the_comic.html');
_menu_item("/0ther/website/hi-res/2021-07-14_contribute-D-beta-reading_by-David-Revoy.jpg", ''._("Beta-reading Forum").'', "https://framagit.org/peppercarrot/webcomics/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=future%20episode");
_menu_item("/0ther/website/hi-res/2021-07-14_contribute-E-wiki_by-David-Revoy.jpg", ''._("Wiki: Document the Universe").'', ''.$root.'/'.$lang.'/wiki/index.html');
_menu_item("/0ther/website/hi-res/2021-07-14_contribute-F-cosplay_by-David-Revoy.jpg", ''._("Cosplay").'', "https://www.davidrevoy.com/tag/cosplay");
_menu_item("/0ther/website/hi-res/2021-07-14_contribute-G-files_by-David-Revoy.jpg", ''._("Sources explorer").'', ''.$root.'/'.$lang.'/files/index.html');
_menu_item("/0ther/website/hi-res/2021-07-14_contribute-H-framagit_by-David-Revoy.jpg", ''._("Git repositories").'', 'https://framagit.org/peppercarrot');

echo '  </article>'."\n";
echo ''."\n";
echo '  <div style="clear:both"></div>'."\n";
echo '  <br>'."\n";
echo '  <br>'."\n";
echo '  <br>'."\n";
echo '</div>'."\n";
echo ''."\n";
?>
