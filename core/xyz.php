<?php if ($root=="") exit;

# Hardcode a display size limit for XYZ
$img_width = "1200px";

# Library: a common error message
function _ErrorMessage($errortitle) {
  global $root;
  global $sources;
  echo '<section class="col sml-12 sml-centered" style="max-width: 1120px; text-align: center;">'."\n";
  echo '<h1 style="text-align: center; color: black; text-shadow: 1px 0px 30px blue, 1px 0px 20px cyan, 1px 0px 10px cyan, 1px 0px 5px white, 1px 0px 5px white;">'.$errortitle.'</h1><br/><br/>'."\n";
  echo '<img src="'.$root.'/'.$sources.'/0ther/artworks/low-res/2019-10-17_chicory_by-David-Revoy.jpg" /><br/>'."\n";
  echo '</section>'."\n";
}

# Find potential new episode and scan jpgs pages in low-res/ subdir
# Note: new episode are directories consistent with pattern "new-ep[0-9][0-9]*"
$allpages = glob(''.$sources.'/new-ep[0-9][0-9]*/low-res/'.$lang.''.$credits.'E[0-9][0-9]P[0-9][0-9].*');
if (empty($allpages)) {
  # Fallback to hardcoded main language (French for XYZ) if no translation are available for active $lang
  $allpages = glob(''.$sources.'/new-ep[0-9][0-9]*/low-res/fr'.$credits.'E[0-9][0-9]P[0-9][0-9].*');
}


# Sanitize allpages array
sort($allpages);

# Retrieve path to new episode
$newepisodepathinfo = pathinfo($allpages[0]);
$newepisodepath = $newepisodepathinfo['dirname'];
$newepisodepath = str_replace('/low-res', '', $newepisodepath);

# Retrieve background color from info.json
$episode_bgcolor = "#FFF";
$episode_info = array();
$episode_info = json_decode(file_get_contents(''.$newepisodepath.'/info.json'), true);
$episode_bgcolor = $episode_info["background-color"];

echo '<div class="containercomic" style="background:'.$episode_bgcolor.';">'."\n";
echo '  <div style="clear:both"></div>'."\n";

# Include the language selection menu
include($file_root.'core/mod-menu-lang.php');


# xyz subdirectories: build a menu to access them.
# They are temporary directories storing previous xyz version during development of episode
# (eg. alpha, alpha2, beta, etc)
$xyzsubdirpath = ''.$newepisodepath.'/xyz';
$hide = array('.', '..', '.directory');
if(is_dir($xyzsubdirpath)) {
	$xyzsubdirectories = array_diff(scandir($xyzsubdirpath), $hide);
	sort($xyzsubdirectories);
}

if (!empty($xyzsubdirectories)) {
  echo '<section class="col sml-12 sml-centered" style="max-width: 1120px; text-align: center;">'."\n";
  echo '  <div style="clear:both"></div>'."\n";
  echo '  <br/>'."\n";

  # Home button (display realtime XYZ, index)
  $xyzindexbuttonbg = 'background: rgba(0,100,160,0.3);';
  if ( $content == "index" OR $content == "" ) {
      $xyzindexbuttonbg = 'background: rgba(0,140,220,0.6);';
  }
  echo '<a href="'.$root.'/'.$lang.'/xyz/index.html" class="translabutton" style="'.$xyzindexbuttonbg.' margin-bottom: 8px;" alt="" title="XYZ version in realtime (last upload)"/>Index</a>&nbsp;'."\n";
  # we loop on found episodes
  foreach ($xyzsubdirectories as $dirpath) {
    # Menu
    $dirname = basename($dirpath);
    # Check if current directory is active
    $xyzbuttonbg = 'background: rgba(0,100,160,0.3);';
    if ( $dirname == $content ) {
      $xyzbuttonbg = 'background: rgba(0,140,220,0.6);';
    }
    echo '<a href="'.$root.'/'.$lang.'/xyz/'.$dirname.'.html" class="translabutton" style="'.$xyzbuttonbg.' margin-bottom: 8px;"><img width="16px" height="16px" src="'.$root.'/core/img/history_b.svg" alt="" title="Previous XYZ version"/> '.$dirname.'</a>&nbsp;'."\n";
  }
  echo '</section>'."\n";
}

# Disclaimer
echo '  <div style="clear:both"></div>'."\n";
echo '  <br>'."\n";
echo '    <div class="xyz"><b> XYZ area: spoiler alert! Please, do not reshare this page</b>:<br/> This episode is still in development and is not meant to be ready for public. It\'s published here only to help proofreader and contributors of Pepper&Carrot. If you want to help and give a feedback, <a href="https://framagit.org/peppercarrot/webcomics/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=future%20episode">join our latest thread on Framagit here.</a></div>'."\n";
echo '  <br>'."\n";


# Secret: If a dummy file named "secret" exists at root of episode directory, do not list the pages.
# This system was made to hide everything from lurker during too early story development (before alpha).
$secrettoken = ''.$newepisodepath.'/secret';
if (!file_exists($secrettoken)) {

  if( $content == 'index' OR $content == '') {
    $pagestodisplay = $allpages;
  } else {
    # XYZ subdirectory (save state)
    $pagestodisplay = glob(''.$sources.'/new-ep[0-9][0-9]*/xyz/'.$content.'/*.jpg');
  }

  # XYZ: Display the header
  if (!empty($pagestodisplay)) {
    $titlepage = ''.$root.'/'.$pagestodisplay[0].'';
    if (file_exists($pagestodisplay[0])) {
      echo '  <article class="panel" style="max-width:'.$img_width.'; margin: 0 auto;" align="center">'."\n";
      echo '    <img class="comicpage" src="'.$titlepage.'" alt="'._("Header").'" /></a>'."\n";
      echo '  </article>'."\n";
      echo ''."\n";
      # Remove the page from the array
      unset($pagestodisplay[0]);
    }

    # Display the comic pages
    foreach ($pagestodisplay as $key => $page) {
      $pagepath = ''.$root.'/'.$page.'';
      if (file_exists($page)) {
        echo '  <article class="panel" style="max-width:'.$img_width.'; margin: 0 auto;">'."\n";

        $title_alt = sprintf(_("Page %d"), $key);
        $comic_alt = $header_title.', '.$title_alt;

        # Gif: special rule to upscale them
        if (strpos($pagepath, 'gif') == true) {
          echo '    <img class="comicpage" style="padding-bottom: 20px;" width="92%" src="'.$pagepath.'" alt="'.$comic_alt.' title="'.$title_alt.'" "/>'."\n";
        } else {
        # Regular comic page
          echo '    <img class="comicpage" src="'.$pagepath.'" alt="'.$comic_alt.'" title="'.$title_alt.'" />'."\n";
        }
        echo '  </article>'."\n";
      }
    }
  } else {
    _ErrorMessage("XYZ subdirectory not found.");
  }
} else {
  _ErrorMessage("XYZ subdirectory not available.");
}

# Container end
echo ''."\n";
echo '  <br>'."\n";
echo '  <br>'."\n";
echo '  <br>'."\n";
echo '</div>'."\n";
echo ''."\n";
?>
