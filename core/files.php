<?php if ($root=="") exit; 

# Initiate page
echo '<div class="container">'."\n";

# Include the webcomic sources 
include($file_root.'core/mod-menu-lang.php');
include($file_root.'core/lib-gallery.php');

# Sidebar
# -------
echo '  <aside class="wikivertimenu col sml-12 med-12 lrg-2" role="complementary">'."\n";
echo '    <nav class="wikibuttonmenu col sml-12 med-12 lrg-12" style="padding:0 0;">'."\n";

# Function: Print a menu entry
# $label = i18n label
# $target = directory filename on 0_sources/0ther
# $icon = filename of the leading icon in /core/img/ (without svg extension)
function _menu_item($label, $target, $icon) {
  global $content;
  global $lang;
  global $root;
  # Manage active status for the button
  preg_match('/.*\/(.*)/', $target, $activepage);
  if ($activepage[1] == $content){
    $dirclass = ' active';
  } else {
    $dirclass = '';
  }
  echo '      <a class="wikibutton'.$dirclass.'" href="'.$root.'/'.$lang.'/'.$target.'.html">'."\n";
  echo '        <img width="20" height="20" src="'.$root.'/core/img/'.$icon.'.svg"/>&nbsp;'.$label.''."\n";
  echo '      </a>'."\n";
}

# Sidebar menu
# ============
# Dynamic services:
echo ''."\n";

_menu_item(_("All episodes"),         "files/episodes",         "directory");
_menu_item(_("All Comic Panels"), "files/all-comic-panels", "directory");

# Gallery of thumbnails:
# Note: $other_directories is an array defined on index.php
_menu_item($other_directories['book-publishing'],  "files/book-publishing",  "directory");
_menu_item($other_directories['eshop'],            "files/eshop",            "directory");
_menu_item($other_directories['press'],            "files/press",            "directory");
_menu_item($other_directories['references'],       "files/references",       "directory");
_menu_item($other_directories['vector'],           "files/vector",           "directory");
_menu_item($other_directories['wallpapers'],       "files/wallpapers",       "directory");
_menu_item($other_directories['wiki'],             "files/wiki",             "directory");

# Link to external galleries:
_menu_item($other_directories['framasoft'],        "artworks/framasoft",        "directory_link");
_menu_item($other_directories['comissions'],       "artworks/misc",             "directory_link");
_menu_item($other_directories['artworks'],         "artworks/artworks",      "directory_link");
_menu_item($other_directories['sketchbook'],       "artworks/sketchbook",    "directory_link");
_menu_item($other_directories['misc'],             "artworks/misc",          "directory_link");
_menu_item($other_directories['fan-art'],          "fan-art/artworks",       "directory_link");
_menu_item(_("Fan comics"),       "fan-art/fan-comics",     "directory_link");
_menu_item(_("Fan Fiction"),      "fan-art/fan-fiction",    "directory_link");

echo '    </nav>'."\n";
echo '    <div style="clear:both"></div>'."\n";
echo '  </aside>'."\n";
echo ''."\n";

# Main content
# ============
echo '  <article class="col sml-12 med-12 lrg-10 sml-text-center">'."\n";

# Gallery mode
# ------------
# list the thumbnails in 0ther/'.$content.'/low-res/
if(is_dir($sources.'/0ther/'.$content.'/low-res/')) {
  echo '    <ul id="gallery">'."\n";

  _gallery($content);

  echo '    </ul>'."\n";

# Episodes mode
# -------------
# list episodes and link them to see their sources (on webcomic.php)
} else if ( $content == "episodes" ) {

  $all_episodes = $episodes_list;
  asort($all_episodes);
  echo '    <ul id="gallery">'."\n";
  foreach ($all_episodes as $episode_directory) {
    $episode_number = preg_replace('/[^0-9.]+/', '', $episode_directory);
    $cover_path = ''.$sources.'/'.$episode_directory.'/low-res/gfx-only/gfx'.$credits.'E'.$episode_number.'.jpg';
      echo '      <li>'."\n";
      echo '        <a href="'.$root.'/'.$lang.'/webcomic/'.$episode_directory.'__sources.html">'."\n";
      echo '        '._img($cover_path, $episode_directory, 450, 320, 40).''."\n";
      echo '        <span class="label">'.$episode_directory.'</span>'."\n";
      echo '        </a>'."\n";
      echo '      </li>'."\n";
  }
  echo '      <li></li>'."\n";
  echo '      <li></li>'."\n";
  echo '      <li></li>'."\n";
  echo '    </ul>'."\n";

# All comic panels mode 
# ---------------------
# ex 'overview' mode; thumbnail of all the pages without text, linked to their hi-resolution
# a pagecount is here to help work of desktop publishing
} else if ( $content == "all-comic-panels" ) {

  # all-comic-panels (ex. "overview")
  $overviewpagecount = 0;
  $all_episodes = $episodes_list;
  asort($all_episodes);
  echo '    <ul id="gallery">'."\n";
  foreach ($all_episodes as $episode_directory) {
    $episode_number = preg_replace('/[^0-9.]+/', '', $episode_directory);
    $search = glob($sources.'/'.$episode_directory.'/low-res/gfx-only/gfx_Pepper-and-Carrot_by-David-Revoy_E[0-9][0-9]P[0-9][0-9].*');
    $key_set = array_keys($search);
    $last_page = end($key_set);
    echo '<div class="col sml-12 med-12 lrg-12 sml-text-center">';
    # beautify name
    $episode_titles = array();
    $episode_titles = json_decode(file_get_contents(''.$sources.'/'.$episode_directory.'/hi-res/titles.json'), true);
    # By default, title of episode is in English
    $humanfoldername = $episode_titles["en"];
    # but if a translation exist
    if(isset($episode_titles[$lang])) {
      # we will prefer using it
      $humanfoldername = $episode_titles[$lang];
    }
    echo '<h3>'.$humanfoldername.'</h3>';
    echo '</div>';
    echo '<section class="col sml-12 med-12 lrg-10 sml-centered sml-text-center">';
    if (!empty($search)) {
      foreach ($search as $key => $filepath) {
          # Exclude the last page of the loop
          if( $key !== $last_page) {
            # weak workaround for excluding page 00 header
            $filepath = str_replace('P00.jpg', 'Pnon-exist.jpg', $filepath);
            # extracting from the path the filename and path itself
            $filename = basename($filepath);
            $fullpath = dirname($filepath);
            if (file_exists($filepath)) {
              # Our page is existing, it exclude the renamed P00.jpg, start the tag
              echo '<figure class="thumbnail col sml-6 med-3 lrg-2" style="min-height:260px"><a href="'.$root.'/'.$sources.'/'.$episode_directory.'/hi-res/gfx-only/'.$filename.'" title="'.$humanfoldername.'" >';
                # it's a real page:
                $overviewpagecount = $overviewpagecount + 1;
                # display a thumbnail
                
                echo '    '._img($root.'/'.$filepath, $humanfoldername, 159, 225, 88).''."\n";
                echo '</a>';
                # Add a page count caption
                echo '<figcaption class="text-center" style="color:#ABABAB">'.$overviewpagecount.'</figcaption>';
                echo '</figure>';
            }
        }
      }
    }
    echo '</section>';
  }

# Index mode 
# ----------
# A cover image, to push the user to click something on the menu
} else {

echo '  <section style="width: 100%; padding: 0 3rem;">'."\n";
echo '    <h2 style="display:block; width: 100%; text-align:center; font-size: 3rem">'._("Sources explorer").'</h2>'."\n";
echo '    <img src="'.$root.'/'.$sources.'/0ther/misc/low-res/2016-05-28_pepper-and-carrot_fixing-the-project_by-David-Revoy.jpg">'."\n";
echo '  </section>'."\n";

}

echo '  </article>'."\n";
echo '</div>'."\n";
echo ''."\n";
?>
