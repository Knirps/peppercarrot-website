<?php if ($root=="") exit;

# This string is used in header, logo info and metadata.
$pepper_and_carrot = _("Pepper&amp;Carrot");
# Add the same suffix for all title in URL
$header_title_full = $header_title.' - '.$pepper_and_carrot.'';
# Meta description of the website, this is what search engines like Google show.
$header_description = _("Official homepage of Pepper&amp;Carrot, a free(libre) and open-source webcomic about Pepper, a young witch and her cat, Carrot. They live in a fantasy universe of potions, magic, and creatures.");
$header_keywords = 'david, revoy, deevad, open-source, comic, webcomic, creative commons, patreon, pepper, carrot, pepper&amp;carrot, libre, artist';

# Display HTML header
# -------------------
echo '<!DOCTYPE html>'."\n";
# $isolang is part of lib-database, and convert a P&C $lang to IETF's BCP 47
# Recommended here: 
echo '<html lang="'.$isolang[$lang].'">'."\n";

# Ascii comment: Carrot welcome the reader of the code
echo '<!--'."\n";
echo '       /|_____|\     ____________________________________________________________'."\n";
echo '      /  \' \' \'  \    |                                                          |'."\n";
echo '     < ( .  . )  >   |  Oh? You read my code? Welcome!                          |'."\n";
echo '      <   °◡    >   <   Full sources on framagit.org/peppercarrot/website-2021  |'."\n";
echo '        \'\'\'|  \      |__________________________________________________________|'."\n";
echo ''."\n";
echo ' Version:'.$version.''."\n";;
echo '-->'."\n";

echo '<head>'."\n";
echo '  <meta charset="utf-8" />'."\n";
echo '  <meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0" />'."\n";

echo '  <meta property="og:description" content="'.$header_description.'"/>'."\n";
echo '  <meta property="og:type" content="article"/>'."\n";
echo '  <meta property="og:site_name" content="'.$pepper_and_carrot.'"/>'."\n";
echo '  <meta property="og:image" content="'.$social_media_thumbnail_URL.'"/>'."\n";
echo '  <meta property="og:image:type" content="image/jpeg" />'."\n";

echo '  <title>'.$header_title_full.'</title>'."\n";
echo '  <meta name="description" content="'.$header_description.'" />'."\n";
echo '  <meta name="keywords" content="'.$header_keywords.'" />'."\n";


echo '  <link rel="icon" href="'.$root.'/core/img/favicon.png" />'."\n";

echo '  <link rel="stylesheet" href="'.$root.'/core/css/framework.css?v='.$version.'" media="screen" />'."\n";
echo '  <link rel="stylesheet" href="'.$root.'/core/css/theme.css?v='.$version.'" media="screen" />'."\n";

# Css exception rules to fix specific display issue with language:
$css_lang = 'core/css/'.$lang.'.css';
if (file_exists($css_lang)) {
  echo '  <link rel="stylesheet" href="'.$root.'/'.$css_lang.'?v='.$version.'" media="screen" />'."\n";
}

# Keyboard arrow navigation, the feature is available only for: webcomics, fan-art comics and galleries (artworks, fan-art and files in viewer mode).
if ($mode == "webcomic" OR  $mode == "viewer" OR $mode == "fan-art") {
  echo '  <script async="" src="'.$root.'/core/js/navigation.js?v='.$version.'"></script>'."\n";
}

echo '  <link rel="alternate" type="application/rss+xml" title="RSS (blog posts)" href="https://www.davidrevoy.com/feed/en/rss" />'."\n";

echo '</head>'."\n";
echo ''."\n";

echo '<body>'."\n";

# This header is not necessary for the viewer:
if ($mode !== "viewer") {

  echo '<header id="header">'."\n";
  echo '  <div class="grid">'."\n";
  echo ''."\n";

  echo '  <div class="title col sml-hide med-hide lrg-show lrg-2 sml-text-left">'."\n";
# Hide the top logo to avoid repetition with the big logo on cover
$logo_visibility = '';
#   if ( $mode == 'homepage' OR $mode == 'webcomic') {
#     $logo_visibility = 'hidden';
#   }
  echo '    <h1 class="'.$logo_visibility.'">'."\n";
  echo '      <a href="'.$root.'/" title="Pepper and Carrot">'."\n";
  if (file_exists('po/'.$lang.'.svg')) {
    echo '        <img class="logo" src="'.$root.'/po/'.$lang.'.svg" alt="'.$pepper_and_carrot.'" title="'.$pepper_and_carrot.'" />';
  } else {
    echo '        <img class="logo" src="'.$root.'/po/en.svg" alt="'.$pepper_and_carrot.'" title="'.$pepper_and_carrot.'" />';
  }
  echo '      </a></h1>'."\n";
  echo '    '."\n";
  echo '  </div>'."\n";

  echo '  <div class="topmenu col sml-7 med-9 lrg-8 med-text-left lrg-text-center">'."\n";
  echo '    <nav class="nav">'."\n";
  echo '      <div class="responsive-menu">'."\n";
  echo '        <label for="menu">'."\n";
  echo '          <img class="burgermenu" src="'.$root.'/core/img/menu.svg" alt=""/>'."\n";
  if (file_exists('po/'.$lang.'.svg')) {
    echo '          <img class="logo" src="'.$root.'/po/'.$lang.'.svg" alt="'.$pepper_and_carrot.'" title="'.$pepper_and_carrot.'" />';
  } else {
    echo '          <img class="logo" src="'.$root.'/po/en.svg" alt="'.$pepper_and_carrot.'" title="'.$pepper_and_carrot.'" />';
  }
  echo '        </label>'."\n";
  echo '        <input type="checkbox" id="menu">'."\n";
  echo '        <ul class="menu expanded">'."\n";
  # ------------ Homepage/webcomics:
                if ($mode == 'homepage') { $class = 'active'; } else { $class = 'no-active'; }
  echo '          <li class="'.$class.'" >'."\n";
  echo '            <a href="'.$root.'/'.$lang.'/"><img class="homeicon" src="'.$root.'/core/img/home.svg" alt="P&C"></a>'."\n";
  echo '          </li>'."\n";
  # ------------ Homepage/webcomics:
                if ($mode == 'webcomics' or $mode == 'webcomic') { $class = 'active'; } else { $class = 'no-active'; }
  echo '          <li class="'.$class.'" >'."\n";
  echo '            <a href="'.$root.'/'.$lang.'/webcomics/index.html">'._("Webcomics").'</a>'."\n";
  echo '          </li>'."\n";
  # ------------ Artworks:
                if ($mode == 'artworks') { $class = 'active'; } else { $class = 'no-active'; }
  echo '          <li class="'.$class.'" >'."\n";
  echo '            <a href="'.$root.'/'.$lang.'/artworks/artworks.html">'._("Artworks").'</a>'."\n";
  echo '          </li>'."\n";
  # ------------ Extras:
                if ($mode == 'goodies' OR $mode == 'wallpapers' ) { $class = 'active'; } else { $class = 'no-active'; }
  echo '          <li class="'.$class.'" >'."\n";
  echo '            <a href="'.$root.'/'.$lang.'/goodies/index.html">'._("Goodies").'</a>'."\n";
  echo '          </li>'."\n";
  # ------------ Contribute:
                if ($mode == 'contribute' OR $mode == 'fan-art' OR $mode == 'chat' OR $mode == 'wiki' OR $mode == 'files') { $class = 'active'; } else { $class = 'no-active'; }
  echo '          <li class="'.$class.'" >'."\n";
  echo '            <a href="'.$root.'/'.$lang.'/contribute/index.html">'._("Contribute").'</a>'."\n";
  echo '          </li>'."\n";
  # ------------ About:
                if ($mode == 'about') { $class = 'active'; } else { $class = 'no-active'; }
  echo '          <li class="'.$class.'" >'."\n";
  echo '            <a href="'.$root.'/'.$lang.'/about/index.html">'._("About").'</a>'."\n";
  echo '          </li>'."\n";
  # ------------ License:
                if ($mode == 'license') { $class = 'active'; } else { $class = 'no-active'; }
  echo '          <li class="'.$class.'" >'."\n";
  echo '            <a href="'.$root.'/'.$lang.'/license/index.html">'._("License").'</a>'."\n";
  echo '          </li>'."\n";
  # ------------ Shop:
  echo '          <li class="external">'."\n";
  echo '            <a href="https://www.davidrevoy.com/static9/shop" target="blank">'._("Shop").'  <img src="'.$root.'/core/img/external-menu.svg" alt=""/></a>'."\n";
  echo '          </li>'."\n";
  # ------------ Blog:
  echo '          <li class="external">'."\n";
  echo '            <a href="https://www.davidrevoy.com/blog" target="blank">'._("Blog").' <img src="'.$root.'/core/img/external-menu.svg" alt=""/></a>'."\n";
  echo '          </li>'."\n";
  echo '        </ul>'."\n";
  echo '      </div>'."\n";
  echo '    </nav>'."\n";
  echo '  </div>'."\n";

  echo '  <div class="col sml-5 med-3 lrg-2">'."\n";
  echo '    <div class="patronagebutton">'."\n";
  echo '      <a href="'.$root.'/'.$lang.'/support/index.html">'."\n";
  echo '        '._("Become a patron").''."\n";
  echo '      </a>'."\n";
  echo '    </div>'."\n";
  echo '  </div>'."\n";

  echo '  </div>'."\n";
  echo '<div style="clear:both;"></div>'."\n";
  echo '</header>'."\n";
  echo ''."\n";

  # Top banner to display a message to the full website
  #echo '  <div style="color: #1e1d34; padding: 10px; background-color: #8683e8; border: 1px solid #2d4f7f; text-align: center; ">'."\n";
  #echo '<b>Breaking news</b>: The website has trouble displaying the comics, it\'s reported and Carrot is working on it! Thank you for your patience. <i>~ David</i>'."\n";
  #echo '</div>'."\n";
  #echo ''."\n";

}

?>
