<?php

# The wiki call... lib-wiki. The core of lib-wiki is also used for the documentation repository.
# The main wiki require this 5 variable for customisation. (2 are empty/undefined on purpose).

$repositoryURL = 'https://framagit.org/peppercarrot/wiki';
$mode_wiki = 'wiki';
$datapath = $wiki;
$wikitheme = '';
$wikiicons = '';

include(dirname(__FILE__).'/lib-wiki.php');

?>
