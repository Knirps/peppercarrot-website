<?php if ($root=="") exit; 

# Initiate page
echo '<div class="container" style="max-width: 1280px;">'."\n";

# Include the webcomic sources 
include($file_root.'core/mod-menu-lang.php');

echo '  <article id="blockmenu" class="col sml-12 sml-text-center">'."\n";

function _menu_item($picture_path, $title, $link) {
  global $root;
  global $sources;
  echo '  <figure class="col sml-12 med-6 blockitem">'."\n";
  echo '    <a href="'.$link.'">'."\n";
  echo '        <img src="'.$root.'/'.$sources.'/'.$picture_path.'" />'."\n";
  echo '      <figcaption class="blockcaption">'.$title.'</figcaption>'."\n";
  echo '    </a>'."\n";
  echo '  </figure>'."\n";
}

_menu_item("/0ther/website/hi-res/2021-07-14_goodies-A-wallpapers_by-David-Revoy.jpg", ''._("Free Wallpapers").'', $root.'/'.$lang.'/wallpapers/index.html');
_menu_item("/0ther/website/hi-res/2021-07-14_goodies-B-brush_by-David-Revoy.jpg", ''._("Free Brushes and Resources").'', "https://www.davidrevoy.com/tag/brush");
_menu_item("/0ther/website/hi-res/2021-07-14_goodies-D-tutorial-makingof_by-David-Revoy.jpg", ''._("Free Tutorials and Making-of").'', "https://www.davidrevoy.com/categorie3/tutorials-brushes-extras");
_menu_item("/0ther/website/hi-res/2021-07-14_goodies-C-avatar-generator_by-David-Revoy.jpg", ''._("Free Avatar Generators").'', "https://www.peppercarrot.com/extras/html/2016_cat-generator/");
_menu_item("/0ther/website/hi-res/2021-07-14_goodies-E-linux-install-guide_by-David-Revoy.jpg", ''._("Free Install Guides for Linux and Tablets").'', "https://www.davidrevoy.com/index.php?tag/linux");
_menu_item("/0ther/website/hi-res/2021-07-14_goodies-F-fonts_by-David-Revoy.jpg", ''._("Free Pepper&Carrot Fonts").'', "https://framagit.org/peppercarrot/fonts");


echo '  </article>'."\n";
echo ''."\n";
echo '  <div style="clear:both"></div>'."\n";
echo '  <br>'."\n";
echo '  <br>'."\n";
echo '  <br>'."\n";
echo '</div>'."\n";
echo ''."\n";
?>
