<?php if ($root=="") exit;

echo '<div class="container">'."\n";
echo '  <main class="main grid" role="main">'."\n";
echo '    <section class="col sml-12 med-12 lrg-12 sml-text-center">'."\n";

echo '      <h1>'._("Error 404: Page not found.").'</h1>'."\n";

echo '    <a href="'.$root.'/'.$lang.'/">'."\n";
echo '       '._("Homepage").''."\n";
echo '    </a>&nbsp;|'."\n";
echo '    <a href="'.$root.'/'.$lang.'/webcomics/index.html">'."\n";
echo '       '._("Webcomics").''."\n";
echo '    </a>&nbsp;|'."\n";
echo '    <a href="'.$root.'/'.$lang.'/artworks/artworks.html">'."\n";
echo '       '._("Artworks").''."\n";
echo '    </a>&nbsp;|'."\n";
echo '    <a href="'.$root.'/'.$lang.'/goodies/index.html">'."\n";
echo '       '._("Goodies").''."\n";
echo '    </a>&nbsp;|'."\n";
echo '    <a href="'.$root.'/'.$lang.'/contribute/index.html">'."\n";
echo '       '._("Contribute").''."\n";
echo '    </a>&nbsp;|'."\n";
echo '    <a href="https://www.davidrevoy.com/static9/shop">'."\n";
echo '       '._("Shop").''."\n";
echo '    </a>&nbsp;|'."\n";
echo '    <a href="https://www.davidrevoy.com/blog">'."\n";
echo '       '._("Blog").''."\n";
echo '    </a>&nbsp;|'."\n";
echo '    <a href="'.$root.'/'.$lang.'/about/index.html">'."\n";
echo '       '._("About").''."\n";
echo '    </a>&nbsp;|'."\n";
echo '    <a href="'.$root.'/'.$lang.'/license/index.html">'."\n";
echo '       '._("License").''."\n";
echo '    </a>'."\n";

echo '      <br/>'."\n";
echo '      <br/>'."\n";
echo '      <img src="'.$root.'/'.$sources.'/0ther/artworks/hi-res/2019-10-17_chicory_by-David-Revoy.jpg" /><br/><br/>'."\n";

echo '    </section>'."\n";
echo '  </main>'."\n";
echo '</div>'."\n";
?>
