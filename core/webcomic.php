<?php if ($root=="") exit;

# Resolution directory
# - $option is an array extracted from URL (index.php)
if (in_array("hd", $option)) {
  $resdirectory = "hi-res";
  $wrapperwidth = "100%";
} else {
  $resdirectory = "low-res";
  $wrapperwidth = "1200px";
}
$wrapperoption = 'style="width:'.$wrapperwidth.';"';

# If the page is in "Source" mode, start with a simple container.
# If not, decorate the background with loading the color of the episode
# written inside a info.json doc at root of the episode.
if (in_array("sources", $option)) {

  echo '<div class="containercomic">'."\n";

} else {
  # Retrieve background color from info.json
  $episode_bgcolor = "#FFF";
  $episode_info = array();
  $episode_info = json_decode(file_get_contents(''.$sources.'/'.$epdirectory.'/info.json'), true);
  $episode_bgcolor = $episode_info["background-color"];
  # If the background color is white, it is probably unspecified
  if ($episode_bgcolor == "#FFF" OR $episode_bgcolor == "#fff" OR $episode_bgcolor == "#FFFFFF" OR $episode_bgcolor == "#ffffff"){
    # then lets make the light / dark mode control the surrounding.
    echo '<div class="containercomic bgcomic">'."\n";
    $wrapperoption = 'style="width:'.$wrapperwidth.';background:'.$episode_bgcolor.';"';
  } else {
    # a color is specified specific for the episode, we respect it:
    echo '<div class="containercomic" style="background:'.$episode_bgcolor.';">'."\n";
  }
}

# Include the language selection menu, and the library to get the credit of translator on top.
include($file_root.'core/mod-menu-lang.php');
include($file_root.'core/lib-credits.php');

# A tight wrapper just around the pages, for 'linking' the pages with a unified white background
# when the mode is dark and background color not defined by the comic.
echo '<div class="comicmonopagebg" '.$wrapperoption.'>'."\n";

# TOOLS
# -----

# Array of all pages
$allpages = glob(''.$sources.'/'.$epdirectory.'/'.$resdirectory.'/'.$lang.''.$credits.'E[0-9][0-9]P[0-9][0-9].*');
if (empty($allpages)) {
  # - fallback to hardcoded English if no translation are available for $lang
  $allpages = glob(''.$sources.'/'.$epdirectory.'/'.$resdirectory.'/en'.$credits.'E[0-9][0-9]P[0-9][0-9].*');
  $fallbackmode = 1;
  $comiclang = 'en';
} else {
  $fallbackmode = 0;
  # In case we fall back to English for an episode that has not been translated yet,
  # Track the comic language in a separate variable
  $comiclang = $lang;
}
sort($allpages);

# Transcript: display the transcript for the current $page
function _transcript($page) {

  global $root;
  global $sources;
  global $comiclang;
  global $option;
  global $content;

  # Extract from $page only E[0-9][0-9]P[0-9][0-9]
  $pageid = basename($page);
  preg_match('/(E[0-9][0-9]P[0-9][0-9])/', $pageid, $matches);
  $pageid = $matches[0];
  # Construct pattern of target transcript file
  $transcriptpage = ''.$sources.'/'.$content.'/hi-res/html/'.$comiclang.'_'.$pageid.'.html';

  # Check if the file exists
  if (file_exists($transcriptpage)) {
    # Check if transcript is requested in URL
    if (in_array("transcript", $option)) {

      # Display transcript html as it is
      echo '    <div class="transcript">'."\n";
      readfile($transcriptpage);
      echo '    </div>'."\n";

    } else {

      # If not requested, show hidden text anyway (for screen readers)
      $transcriptdata = file_get_contents($transcriptpage);
      # Change description list entries into paragraphs to make screen readers less noisy
      $transcript_text = '';
      $lines = explode("\n", $transcriptdata);
      foreach ($lines as $line) {
        $patterns = array(
          '/<dt><strong>(.*)<\/strong><\/dt>/ui',
          '/<dd>(.*)<\/dd>/ui'
        );
          $replacements = array(
          '\\1: ',
          '\\1.<br />'
        );
        $line = preg_replace($patterns, $replacements, trim($line));
        $transcript_text .= $line;
      }
      $transcript_text = trim(preg_replace('/\s*<dl>(.*)<\/dl>\s*/i', '\\1', $transcript_text));
      print('   <div class="hidden">' . $transcript_text . '</div>');
    }
  }
}

$trtitle = ''._("Display the transcript for this episode.").'';

# Hd button special rules
if (in_array("hd", $option)) {
  $hdclass = ' active';
  $gif_width = '2265px';
  $gif_padding = '22px';
} else {
  $hdclass = '';
  $gif_width = '1100px';
  $gif_padding = '32px';
}
# transcript button: highlight management
if (in_array("transcript", $option)) {
  $trclass = ' active';
} else {
  $trclass = '';
}
# Sources button special rules
if (in_array("sources", $option)) {
  $srclass = ' active';
} else {
  $srclass = '';
}
$html_transcript_search = glob(''.$sources.'/'.$epdirectory.'/hi-res/html/'.$comiclang.'_E[0-9][0-9]P00.html');
if (empty($html_transcript_search)) {
  $trtitle = ''._("No transcript available for this episode.").'';
  $trclass = ' off';
}

# Create the index of all episode for the navigation.
$episode_index = array();
$all_episodes = glob(''.$sources.'/ep[0-9][0-9]*');
sort($all_episodes);
foreach ($all_episodes as $key => $episode_directory) {
  $episode_directory = basename($episode_directory);
  array_push($episode_index, $episode_directory);

}
sort($episode_index);

# DISPLAY
# -------

# Display options
echo '  <nav class="comicoptions col sml-12 sml-text-center">'."\n";
echo '    <a class="switch '.$hdclass.'" href="'.$root.'/'.$lang.'/'.$mode.'/'.$content.''._newoption("hd").'.html" title="'._("Display the pages of this episode in high resolution.").'">'._("High resolution").'</a>'."\n";
echo '    <a class="switch '.$trclass.'" href="'.$root.'/'.$lang.'/'.$mode.'/'.$content.''._newoption("transcript").'.html" title="'.$trtitle.'">'._("Transcript").'</a>'."\n";
echo '    <a class="download '.$srclass.'" href="'.$root.'/'.$lang.'/'.$mode.'/'.$content.''._newoption("sources").'.html" title="'._("Display the full set of sources files for this episode in this language.").'">'._("Sources and license").'</a>'."\n";

# Fallback message if translation doesn't exist
if ( $fallbackmode == 1 ){
  echo '    <div class="notification">'."\n";
  echo '        '._("Oops! There is no translation available yet for this episode with the language you selected. The page will continue in English.").''."\n";
  echo '    </div>'."\n";
  echo '    <br/>'."\n";
  echo '    <div class="button">'."\n";
  echo '        <a href="'.$transladocumentationlink.'">+ '._("Add a translation").'</a>'."\n";
  echo '    </div>'."\n";
}
echo '  </nav>'."\n";
echo ''."\n";


# Display Sources
if (in_array("sources", $option)) {
  echo '  <div class="col sml-12 sml-text-center" style="margin-top: 28px">'."\n";
  echo '    '._navigation($epdirectory, $episode_index, $mode).'';
  echo '  </div>'."\n";
  echo ''."\n";

  # Include the webcomic sources
  include($file_root.'core/mod-webcomic-sources.php');

# Display the Webcomic episode
} else {

  # Display header
  if (!empty($allpages)) {
    $titlepage = ''.$root.'/'.$allpages[0].'';
    if (file_exists($allpages[0])) {
      echo '  <div class="panel" align="center">'."\n";
      echo '    <img class="comicpage" src="'.$titlepage.'" alt="'._("Header").'">'."\n";
      echo '    '._transcript($allpages[0]).''."\n";
      echo '  </div>'."\n";
      echo ''."\n";
      # Remove the page from the array
      unset($allpages[0]);
    }

    # From lib-credits.php
    echo '  <div class="translacredit col sml-12 sml-centered">'."\n";
    _print_translation_only_credits($lang, $epdirectory);
    echo '  </div>'."\n";


    # Navigation top
    echo ''._navigation($epdirectory, $episode_index, $mode).'';

    # Display the comic pages
    foreach ($allpages as $key => $page) {
      $pagepath = ''.$root.'/'.$page.'';
      if (file_exists($page)) {
        echo '  <article class="panel">'."\n";

        $title_alt = sprintf(_("Page %d"), $key);
        $comic_alt = $header_title.', '.$title_alt;

        # Gif: special rule to upscale them
        if (strpos($pagepath, 'gif') == true) {
          echo '    <img class="comicpage" style="max-width:'.$gif_width.';padding-bottom: '.$gif_padding.';" width="92%" src="'.$pagepath.'" alt="'.$comic_alt.' title="'.$title_alt.'" "/>'."\n";
        } else {
        # Regular comic page
          echo '    <img class="comicpage" src="'.$pagepath.'" alt="'.$comic_alt.'" title="'.$title_alt.'" />'."\n";
        }

        echo '      '._transcript($page).''."\n";
        echo '  </article>'."\n";
      }
    }
  }
  echo ''."\n";

  # Display a button to show comments
  $episode_number = preg_replace('/[^0-9.]+/', '', $epdirectory);
  $episode_comments = array();
  $episode_comments = json_decode(file_get_contents(''.$sources.'/comments.json'), true);
  $comments_info = $episode_comments['ep'.$episode_number];
  $comments_url = $comments_info["url"];
  $comments_number = $comments_info["nb_com"];

  echo '  <div class="col sml-12 sml-text-center">'."\n";

  # Add a 'Become a patron' button
  echo '    <a class="patronbutton" href="'.$root.'/'.$lang.'/support/index.html">'."\n";
  echo '      '._("Become a patron").''."\n";
  echo '    </a>'."\n";

# a temporary button for Pepper&Carrot mini and Morevna, until we make a fair feature for all fan content
  $pcminiepisodes=array('22'=>'02', '31'=>'24', '32'=>'26', '33'=>'27', '34'=>'28', '35'=>'29', '36'=>'30', '37'=>'31', '38'=>'32');
  $pcminibutton="Read episode of the Fan-comic Pepper&Carrot mini";
  $morevnabutton="Watch a video adaptation of this episode by Morevna on Youtube";
  $morevnalist=array('03'=>'https://www.youtube.com/watch?v=nuw1B334feI&list=PLuMV0wvPXrPBuSOG6ghvwnl3z7oIJcEEK&index=1', 
     '04'=>'https://www.youtube.com/watch?v=-wWIZ7wT8eg&list=PLuMV0wvPXrPBuSOG6ghvwnl3z7oIJcEEK&index=4', 
     '05'=>'https://www.youtube.com/watch?v=v6-r01Jb_HM&list=PLuMV0wvPXrPBuSOG6ghvwnl3z7oIJcEEK&index=2', 
     '06'=>'https://www.youtube.com/watch?v=cTb1_w8hvqY&list=PLuMV0wvPXrPBuSOG6ghvwnl3z7oIJcEEK&index=3');
  if ($lang=='fr'){
     $pcminibutton="Lire l'épisode de BD de fan Pepper&Carrot mini";
     $morevnabutton="Regardez une adaptation vidéo de cet épisode par Morevna sur YT";
     $morevnalist=array('03'=>'https://www.youtube.com/watch?v=l28j_U269iQ', 
        '04'=>'https://www.youtube.com/watch?v=lfx0aWoRx1I', 
        '06'=>'https://www.youtube.com/watch?v=WZdkLJLD2xI');
     }
  if ($lang=='ru'){
     $pcminibutton="Читать эпизод фан-комикса Pepper&Carrot mini";
     $morevnabutton="Смотрите видеоадаптацию этого эпизода от Morevna на Youtube";
     $morevnalist=array('03'=>'https://www.youtube.com/watch?v=ynNxFjTMDUY&list=PLuMV0wvPXrPCF5Xc7wsjIwb62LZG-x58V&index=1', 
        '04'=>'https://www.youtube.com/watch?v=Dnj7nLzhkjo', 
        '05'=>'https://www.youtube.com/watch?v=KkJP-ufZYuc&list=PLuMV0wvPXrPCF5Xc7wsjIwb62LZG-x58V&index=2', 
        '06'=>'https://www.youtube.com/watch?v=iY2cuLJHAAA&list=PLuMV0wvPXrPCF5Xc7wsjIwb62LZG-x58V&index=3');
     }
  if ($lang=='sl'){$pcminibutton="Beri epizodo stripa oboževalca Pepper&Carrot mini";}
  if ($lang=='de'){$pcminibutton="Lies eine Episode von Fan Comic Pepper&Carrot mini";}
  if ($lang=='es' or $lang=='mx'){$pcminibutton="Lee episodio del Cómic de un seguidor: Pepper&Carrot mini";}
  if ($lang=='fa'){$pcminibutton="قسمت‌های داستان مصور «فلفل و هویج - نسخهٔ کوچک‌تر» که توسط طرفداران خلق شده‌است را بخوانید.";}
  if (array_key_exists($episode_number,$morevnalist) and in_array($lang, array('en','fr','ru')) ){
    echo '    <a class="pcminilinkbutton" href="'.$morevnalist[$episode_number].'" title="'.$morevnabutton.'">'.$morevnabutton.'</a>';}
  if (array_key_exists($episode_number,$pcminiepisodes) ){
    if (file_exists("0_sources/0ther/community/Pepper-and-Carrot-Mini_by_Nartance/".$lang."_PCMINI_E".$pcminiepisodes[$episode_number].'P01_by-Nartance.jpg')){
      echo '    <a class="pcminilinkbutton" href="'.$root.'/'.$lang.'/fan-art/fan-comics__Pepper-and-Carrot-Mini_by_Nartance~~'.$lang.'_PCMINI_E'.$pcminiepisodes[$episode_number].'P01_by-Nartance.html" title="'.$pcminibutton.'">'.$pcminibutton.'</a>';
}} # end of temporary button

  echo '    <a class="commentbutton" href="'.$comments_url.'/show#comments" title="'.
    sprintf(ngettext('Read the %d comment on the blog.', 'Read the %d comments on the blog.', $comments_number),$comments_number).'">'.
    sprintf(ngettext('Read %d comment.', 'Read %d comments.', $comments_number),$comments_number).'</a>';


  # Add a graph for arrow-navigation on keyboard for discoverability of the feature:
  echo '  <br/>'."\n";
  echo '    <img width="120" height="80" src="'.$root.'/core/img/arrows-navigation.svg" alt="arrow navigation"/>'."\n";
  echo ''."\n";

  echo '    </div>'."\n";
  echo '  </div>'."\n";
  echo ''."\n";

  # Navigation bottom
  echo '<br/>'."\n";
  echo ' '._navigation($epdirectory, $episode_index, $mode).'';
  echo ''."\n";


}

# Container end
echo ''."\n";
echo '  <br>'."\n";
echo '  <br>'."\n";
echo '  <br>'."\n";
echo '</div>'."\n";
echo ''."\n";
?>
