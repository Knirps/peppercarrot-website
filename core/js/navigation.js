const hotkeys = {
  "arrowleft": "prevnavbutton",
  "arrowright": "nextnavbutton",
  "ctrl+arrowleft": "firstnavbutton",
  "ctrl+arrowright": "lastnavbutton",
}

window.addEventListener('keydown', function(event) {
  if (event.target == document.body) { //filtering inputs from e.g. textareas and stuff
    const hotkey =
      //not adding meta key because it's quirky
        (event.ctrlKey ? "ctrl+" : "")
      + (event.altKey ? "alt+" : "")
      + (event.shiftKey ? "shift+" : "")
      + event.key.toLowerCase();

    if (hotkey in hotkeys) {
      event.preventDefault();

      let toClick = document.getElementsByClassName(hotkeys[hotkey]);

      if (toClick.length != 0 && !toClick[0].classList.contains('off')) {
        toClick[0].click();
      }
    }
  }
});
