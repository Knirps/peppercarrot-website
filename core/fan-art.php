<?php if ($root=="") exit;

# Initiate page
echo '<div class="container">'."\n";

# Include the webcomic sources
include($file_root.'core/mod-menu-lang.php');
include($file_root.'core/lib-gallery.php');

$number_fanart = _gallery_number("fan-art");
$chatroom = $root.'/'.$lang.'/chat/index.html';

# Check and clean our options:
$option_get = implode("-", $option);
$option_clean = str_replace('~~', '/', $option_get);
$option_clean = str_replace('..', '', $option_clean);

# Sidebar
# -------
echo '  <aside class="wikivertimenu col sml-12 med-12 lrg-2" role="complementary">'."\n";
echo '    <nav class="wikibuttonmenu col sml-12 med-12 lrg-12" style="padding:0 0;">'."\n";

# Array of directories available
$sources_directories = glob($sources.'/0ther/*');
sort($sources_directories);

$dirclass = '';

# Menu
if ($content == 'artworks') { $dirclass = ' active'; } else { $dirclass = ' no-active'; }
echo '      <a class="artbutton fanart'.$dirclass.'" href="'.$root.'/'.$lang.'/fan-art/artworks.html">'."\n";
echo '        '.sprintf(ngettext('%d Fan-art', '%d Fan-art', $number_fanart),$number_fanart).''."\n";
echo '      </a>'."\n";

if ($content == 'fan-comics') { $dirclass = ' active'; } else { $dirclass = ' no-active'; }
echo '      <a class="artbutton fancomics'.$dirclass.'" href="'.$root.'/'.$lang.'/fan-art/fan-comics.html">'."\n";
echo '        '._("Fan comics").''."\n";
echo '      </a>'."\n";

if ($content == 'fan-fiction') { $dirclass = ' active'; } else { $dirclass = ' no-active'; }
echo '      <a class="artbutton fanfiction'.$dirclass.'" href="'.$root.'/'.$lang.'/fan-art/fan-fiction.html">'."\n";
echo '        '._("Fan Fiction").''."\n";
echo '      </a>'."\n";

echo '<br/>';
echo '<div style="display: inline-block">';
echo '<b>'._("How to:").'</b><br/>';
echo ''.sprintf(_("Send me your artworks, comics and fiction using social-medias (tag me, my social media links are written in the footer of this website) or send them to me on <a href=\"%s\">the chat room</a> and I'll repost them here. "),$chatroom).'';
echo '</div>';
echo '    </nav>'."\n";
echo '    <div style="clear:both"></div>'."\n";
echo '  </aside>'."\n";
echo ''."\n";


# Container start
echo '  <article class="col sml-12 med-12 lrg-10 sml-text-center">'."\n";


# FAN FICTION
# ------------
if ($content == 'fan-fiction') {

  $repositoryURL = "https://framagit.org/peppercarrot/scenarios";
  $datapath = $scenario;

  echo '<section class="page col sml-12 sml-centered">'."\n";

  $pattern = $datapath.'/'.$option_clean.'.md';
  if (file_exists($pattern)) {

    # Start edit buttons
    echo '     <div class="edit" >'."\n";

    echo '       <div class="button moka" >'."\n";
    echo '        <a href="'.$repositoryURL.'/commits/master/'.$option_clean.'.md" target="_blank" title="'._("External history link to see all changes made to this page").'" >'."\n";
    echo '          <img width="16px" height="16px" src="'.$root.'/core/img/history.svg" alt=""/>'."\n";
    echo '            '._("View history").''."\n";
    echo '        </a>'."\n";
    echo '       </div>'."\n";

    echo '       <div class="button moka" >'."\n";
    echo '        <a href="'.$repositoryURL.'/edit/master/'.$option_clean.'.md" target="_blank" title="'._("Edit this page with an external editor").'" >'."\n";
    echo '          <img width="16px" height="16px" src="'.$root.'/core/img/edit.svg" alt=""/>'."\n";
    echo '            '._("Edit").''."\n";
    echo '        </a>'."\n";
    echo '       </div>'."\n";

    echo '     </div>'."\n";

    # Display the page
    include(dirname(__FILE__).'/lib-parsedown.php');
    $contents = file_get_contents($pattern);
    $Parsedown = new Parsedown();
    echo $Parsedown->text($contents);
    echo '	    <br/><br/>'."\n";

  } else {

    # Fan Fiction main menu
    echo '<h1>'._("Fan Fiction").'</h1>';

    # Fan-fictions subtitles categories
    $fanfiction_folders = array('01_novels'=>_("Novels"),
                                '02_Scenarios-for-new-episodes'=>_("Scenarios for new episodes"),
                                '03_community-improvisations'=>_("Community improvisations"),
                                '04_Temporary_Drafts'=>_("Temporary Drafts")
                          );

    foreach ($fanfiction_folders as $dirname => $title) {

      echo '<br/><h2>'.$title.'</h2>';

      $search = glob($datapath."/".$dirname."/*.md");
      if (!empty($search)){
        foreach ($search as $file) {

          # clean path into filename without extension
          $txtlink = basename($file);
          $txtlink = preg_replace('/\\.[^.\\s]{2,4}$/', '', $txtlink);

          # Split prefix (in case of a French novel, right now)
          $prefix = "";
          if (strpos($txtlink, 'fr~') !== false) {
            # Prefix for the novels written by Nartance
            $prefix = '<span class="notes">('._("In French").')</span>';
          }

          # Extract attribution for the author
          preg_match('/.*([-_]by[-_].*)/', $txtlink, $txtattribution_part);
          $txtattribution = str_replace('-', ' ', $txtattribution_part[1]);
          $txtattribution = str_replace('by', '', $txtattribution);
          $txtattribution = str_replace('_', ' ', $txtattribution);
          $txtattribution = str_replace('  ', '', $txtattribution);

          $filelabel = str_replace($txtattribution_part[1], '', $txtlink);
          $filelabel = str_replace('fr~', '', $filelabel);
          $filelabel = str_replace('--', ', ', $filelabel);
          $filelabel = str_replace('_by', '"', $filelabel);
          $filelabel = str_replace('_', ' ', $filelabel);
          $filelabel = str_replace('-', ' ', $filelabel);
          $filelabel = preg_replace('/\\.[^.\\s]{2,4}$/', '', $filelabel);
          $filelabel = ucfirst($filelabel);

          if (substr($file, 0, 1) === '_') {
            # exclude system file starting with '_'.

          } else {

            # filter
            if ($file === 'README.md'){
                # in case of README
            } else if ($file === 'template.md'){
              # filter template, do nothing

            } else {
              # display the result
              $path_to_page = ''.$root.'/'.$lang.'/fan-art/fan-fiction__'.$dirname.'~~'.$txtlink.'.html';
              # %1$s = artwork title, %2$s = author
              echo sprintf(_('%1$s by %2$s'), '<div class="scenariolist">'.$prefix.'&nbsp;<a href="'.$path_to_page.'" title="">'.$filelabel.'</a><span class="notes">', $txtattribution)."</span></div>\n";
            }

          }
        }
      }
    }
  }
  echo '  <br>'."\n";
  echo '  </section>'."\n";

} else if ($content == 'fan-comics') {
  # FAN COMICS
  # ----------
  $pathcommunityfolder = $sources.'/0ther/community';
  $community_subdir_check = $pathcommunityfolder.'/'.$option_clean.'/00_cover.jpg';
  $community_episode_check = $pathcommunityfolder.'/'.$option_clean.'.jpg';

  # Fan Comics Episode listing
  # =========================
  if (file_exists($community_subdir_check)) {
    $folderpath = dirname($community_subdir_check);
    $foldername = basename($folderpath);

    # Display the title of the project and markdown:
    $foldernameclean = str_replace('_', ' ', $foldername);
    $foldernameclean = str_replace('-', ' ', $foldernameclean);
    $foldernamereplace = explode(" by ", $foldernameclean, 2);
    $foldernameclean = sprintf(_('%1$s by %2$s'), $foldernamereplace[0].'</h2><span class="font-size: 0.5rem;">', $foldernamereplace[1]);

    echo '<h2>'.$foldernameclean.'</span>';
    $hide = array('.', '..');
    $mainfolders = array_diff(scandir($folderpath), $hide);
    if (file_exists($folderpath.'/'.$lang.'_infos.md')) {
      $contents = file_get_contents($folderpath.'/'.$lang.'_infos.md');
    } else {
      $contents = file_get_contents($folderpath.'/en_infos.md');
    }
    include(dirname(__FILE__).'/lib-parsedown.php');
    $Parsedown = new Parsedown();
    echo '<div style="max-width: 910px; margin: 0 auto;">';
    echo $Parsedown->text($contents);
    echo '</div>';
    echo '<br/><br/>';


    # Display episodes
    echo '<section class="col sml-12 med-12 lrg-10 sml-centered sml-text-center" style="padding:0 0;">';
    # We look at English P01: first page of each fan-art webcomic; and in English for the thumbnail (the default minimal language to post an episode)
    $search = glob($folderpath.'/en*P01*.jpg');
    rsort($search);
    # we loop on found episodes
    if (!empty($search)){
      foreach ($search as $filepath) {
        # check if translation exists
        $filepathlocale = str_replace('en_', $lang.'_', $filepath);
        if (file_exists($filepathlocale)) { $filepath = $filepathlocale; }
        # episode number extraction
        $filename = basename($filepath);
        $filenameoption = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
        $fullpath = dirname($filepath);
        $filenameclean = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
        $filenameclean = substr($filenameclean, 11); // remove 11 first characters
        $filenameclean =  substr($filenameclean, 0, 2); // keeps two numbers
        $filenameclean = str_replace('_', ' ', $filenameclean);
        $filenameclean = str_replace('-', ' ', $filenameclean);

        echo '<a href="'.$root.'/'.$lang.'/fan-art/fan-comics__'.$foldername.'~~'.$filenameoption.'.html" >';
        echo '<figure class="thumbnail col sml-6 med-4 lrg-4">';
         _img($filepath, $filename, 400, 400, 82);
        echo '<figcaption class="text-center" >';
        echo 'Episode '.$filenameclean.'';
        echo '</figcaption>';
        echo '</figure>';
      }
    }
    echo '</section>';

  # Fan Comics display comics
  # =========================
  } else if (file_exists($community_episode_check)) {
    $jpg_file = basename($community_episode_check);
    $jpg_file_notranslation = substr($jpg_file, 1);
    $folderpath = dirname($community_episode_check);
    $foldername = basename($folderpath);
    $thisepisode_number = '01';

    # Nex-prev buttons
    $episode_index = glob($folderpath.'/'.$lang.'_*E[0-9][0-9]P01*.jpg');
    if (empty($episode_index)){
      $episode_index = glob($folderpath.'/en_*E[0-9][0-9]P01*.jpg');
    }
    $episode_index_formated = array();
    foreach ($episode_index as $episodefullpath) {
      $picture = basename($episodefullpath);
      $picture_withoutext = preg_replace('/\\.[^.\\s]{3,4}$/', '', $picture);
      $folderpath = dirname($episodefullpath);
      $foldername = basename($folderpath);
      $item = $foldername.'~~'.$picture_withoutext;
      array_push($episode_index_formated, $item);
    }
    $current_option = str_replace('/en_', '/'.$lang.'_', $option_get);
    echo ''._navigation($current_option, $episode_index_formated, "fan-art").'';


    # Display the pages
    # Pepper-and-Carrot-Mini_by_Nartance~~en_PCMINI_E22P01_by-Nartance.html
    if (preg_match('/[a-zA-Z\~-]+([0-9]+)P[0-9][0-9][a-zA-Z-]*/', $option_get, $results)) {
      $thisepisode_number = $results[1];
    }


    $pages_to_display = glob($folderpath.'/'.$lang.'_*E'.$thisepisode_number.'P*.jpg');
    if (empty($pages_to_display)){
      $pages_to_display = glob($folderpath.'/en_*E'.$thisepisode_number.'P*.jpg');
    }
    foreach ($pages_to_display as $page) {
      echo '<div>';
      echo '<img src="'.$root.'/'.$page.'">';
      echo '</div>';
    }
    echo ''._navigation($current_option, $episode_index_formated, "fan-art").'';

  # Fan Comics Main menu
  # ====================
  } else {

    echo '<section class="col sml-12 sml-centered" style="margin: 1.2rem 0 0 0;">'."\n";
    $hide = array('.', '..');
    $mainfolders = array_diff(scandir($pathcommunityfolder), $hide);
    sort($mainfolders);

    # we loop on found episodes
    foreach ($mainfolders as $folderpath) {
      # Name extraction
      $filename = basename($folderpath);
      $filenameclean = str_replace('_', ' ', $filename);
      $filenameclean = str_replace('-', ' ', $filenameclean);
      $filenameclean = str_replace('featured', '', $filenameclean);
      $filenamereplace = explode(" by ", $filenameclean, 2);
      $filenameclean = sprintf(_('%1$s by %2$s'), $filenamereplace[0].'</a><br/><span class="detail">', $filenamereplace[1]);
      $filenamezip = str_replace('jpg', 'zip', $filename);
      echo '<figure class="thumbnail col sml-6 med-4 lrg-4">';
      echo '<a href="'.$root.'/'.$lang.'/fan-art/fan-comics__'.$folderpath.'.html" >';
       _img(''.$root.'/'.$pathcommunityfolder.'/'.$folderpath.'/00_cover.jpg', $filename, 400, 400, 82);
      echo '</a><br/>';
      echo '<figcaption class="text-center" >';
      echo '<a href="'.$root.'/'.$lang.'/fan-art/fan-comics__'.$folderpath.'.html" >';
      echo ''.$filenameclean.'</span><br/>';
      echo '</figcaption>';
      echo '<br/><br/>';
      echo '</figure>';
    }
    echo '</section>'."\n";
  }


} else {
# Fan-art gallery
# ---------------
echo '    <ul id="gallery">'."\n";

_gallery("fan-art");

echo '    </ul>'."\n";
echo '  </article>'."\n";
}

# Container end
echo '</div>'."\n";
echo ''."\n";
echo '  <div style="clear:both"></div>'."\n";
echo '  <br>'."\n";
echo '  <br>'."\n";
echo '  <br>'."\n";
echo ''."\n";
?>
