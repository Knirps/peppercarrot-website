<?php if ($root=="") exit;


# Include the language selection menu and credit engine
include($file_root.'core/mod-menu-lang.php');
include($file_root.'core/lib-credits.php');
echo '  <div style="clear:both"></div>'."\n";
echo '<div class="container" style="max-width: 1160px;">'."\n";
echo '  <section class="tos col sml-12 med-12 lrg-10 sml-centered text-center" lang="en">'."\n";

_img($sources.'/0ther/misc/low-res/2016-03-08_fairy-on-rpg-dice-20_by-David-Revoy.jpg', _("Photo of the author, David Revoy with his cat named Noutti while drawing the first episodes of Pepper&Carrot."), 1200, 400, 82);

# Display the page
include(dirname(__FILE__).'/lib-parsedown.php');
$file = "TERMS-OF-SERVICE-AND-PRIVACY.md";
$contents = file_get_contents($file);
$Parsedown = new Parsedown();
echo $Parsedown->text($contents);

echo '  </section>'."\n";
echo ''."\n";
echo '  <div style="clear:both"></div>'."\n";
echo '  <br>'."\n";
echo '  <br>'."\n";
echo '  <br>'."\n";
echo '</div>'."\n";
echo ''."\n";

?>
