<?php if ($root=="") exit; 

# Initiate page
echo '<div class="container" style="max-width: 1280px;">'."\n";

# Include the webcomic sources 
include($file_root.'core/mod-menu-lang.php');

echo '  <div style="clear:both"></div>'."\n";
echo '  <article class="page" style="max-width: 950px; margin: 1.5rem auto;">'."\n";

echo '<h1>Chat rooms</h1>';

echo '<strong>English</strong> is the language used for collaborating on Pepper&Carrot. That\'s also the language used on the official chat rooms.<br/><br/> The main channel uses <strong>the Matrix protocol</strong>. A chat client named Element is available for free and will launch inside your web-browser without requiring any installation once you click the button under. But you\'ll have to create an account to interact:';

echo '<div class="button" style="text-align: center; margin: 2rem auto 0.5rem auto; font-weight: 400;"><big><a href="https://matrix.to/#/#peppercarrot:matrix.org" title="Pepper&amp;Carrot official room on #peppercarrot:matrix.org">&nbsp;&nbsp;&nbsp;Pepper&amp;Carrot chat room on [Matrix]&nbsp;&nbsp;&nbsp;</a></big></div> ';

echo '	 <br/>Many other clients exists for the Matrix protocol, for your desktop computer and your smartphone. <a href="https://matrix.org/clients">You\'ll find a complete list here</a><br/>';
echo '	<br/><br/><b>Alternatives</b>:<br/>- the <strong>IRC</strong> channel (#pepper&amp;carrot on <a href="https://libera.chat/">libera.chat</a>), bridged. <br/> - the <a href="https://telegram.me/+V76Ep1RKLaw5ZTc0" title="A community Telegram channel"> <strong>Telegram</strong> channel</a> (managed by the community), bridged.<br/><br/>';

echo '<span style="display: block; font-size:1.1rem; opacity: 0.7; text-align: center;">Note: the content of this channel is public and the log is saved.<br/> Please read <strong><a href="'.$root.'/'.$lang.'/documentation/409_Code_of_Conduct.html">our Code of Conduct</a></strong> before interacting.</span>';
echo '<br/>';
 _img($sources.'/0ther/sketchbook/low-res/2020-04-17_podcast_by-David-Revoy.jpg', 'Pepper communicates on a microphone.', 900, 500, 82);
echo '  <br><br></article>'."\n";
echo ''."\n";
echo '  <div style="clear:both"></div>'."\n";
echo '  <br>'."\n";
echo '  <br>'."\n";
echo '  <br>'."\n";
echo '</div>'."\n";
echo ''."\n";
?>
