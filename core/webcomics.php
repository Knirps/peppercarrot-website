<?php if ($root=="") exit;

echo '<div class="container">'."\n";

# Include the language selection menu
include($file_root.'core/mod-menu-lang.php');

echo '  <section class="col sml-12">'."\n";
echo '    <h2 style="margin-top: 0;">'._("All episodes").'</h2>'."\n";

# Array of all episodes (copied from database to sort it backward, newer episode on top)
$all_episodes = $episodes_list;
rsort($all_episodes);

$all_episodes_count = count($all_episodes);
$episodes_homepage = array();
$episodes_homepage  = $all_episodes;

// if ($content == "all")  {
//   $episodes_homepage  = $all_episodes;
// } else {
//   $episodes_homepage = array_slice($all_episodes, 0, 6);
// }

$episode_comments = array();
$episode_comments = json_decode(file_get_contents(''.$sources.'/comments.json'), true);

# Display all thumbnails
foreach ($episodes_homepage as $key => $episode_directory) {
  $episode_number = preg_replace('/[^0-9.]+/', '', $episode_directory);
  $cover_path = ''.$sources.'/'.$episode_directory.'/low-res/'.$lang.''.$credits.'E'.$episode_number.'.jpg';
  $episode_link = $root.'/'.$lang.'/webcomic/'.$episode_directory.'.html';

  $episode_info = array();
  $episode_info = json_decode(file_get_contents(''.$sources.'/'.$episode_directory.'/info.json'), true);
  $episode_date = $episode_info["published"];

  $episode_titles = array();
  $episode_titles = json_decode(file_get_contents(''.$sources.'/'.$episode_directory.'/hi-res/titles.json'), true);

  $comments_info = $episode_comments['ep'.$episode_number];
  $comments_url = $comments_info["url"];
  $comments_number = $comments_info["nb_com"];

  $class = '';
  # In case the cover is not available in the current language, fallback to English.
  if (!file_exists($cover_path)) {
    $cover_path = ''.$sources.'/'.$episode_directory.'/hi-res/en'.$credits.'E'.$episode_number.'.jpg';
    $locale_title = $episode_titles["en"];
    # Decorate the thumbnail
    $class = 'notranslation';
  } else {
    $locale_title = $episode_titles[$lang];
    $class = 'translated';
    # Get the title translated
  }
  $cover_description = ''.$locale_title.' '._("(click to open the episode)").'';
  echo '    <figure class="thumbnail '.$class.' col sml-12 med-6 lrg-4">'."\n";
  echo '      <a href="'.$episode_link.'">'."\n";
  echo '        '._img($root.'/'.$cover_path, $cover_description, 480, 399, 89).''."\n";
  echo '      </a>'."\n";
  echo '    <figcaption>'.sprintf(ngettext('Published on %1$s, <a %2$s>%3$d comment</a>.', 'Published on %1$s, <a %2$s>%3$d comments</a>.', $comments_number), $episode_date, 'href="'.$comments_url.'/show#comments" title="'.sprintf(ngettext('Read the %d comment on the blog.', 'Read the %d comments on the blog.', $comments_number),$comments_number).'"', $comments_number).'</figcaption>'."\n";
  echo '    </figure>'."\n";
}
echo ''."\n";
echo '  <div style="clear:both"></div>'."\n";
echo '  <br>'."\n";
echo '  <br>'."\n";
echo '  <br>'."\n";
echo '</div>'."\n";
?>
